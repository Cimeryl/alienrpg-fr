
/************************************************************************************/
Hooks.once('init', () => {

  if(typeof Babele !== 'undefined') {
		
    console.log("BABELE LOADED !!!");
		Babele.get().register({
			module: 'alienrpg-fr',
			lang: 'fr',
			dir: 'compendiums'
		});
  }
});
